package com.azou.fraz.applicationtholdi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


import javax.net.ssl.HttpsURLConnection;

/**
 * Created by fraz on 20/03/2017.
 */

public class WebService extends AsyncTask<URL, Void, Object> {

    private Activity _activity;
    private URL _url;
    private  HttpURLConnection urlConnection;

    @Override
    protected Object doInBackground(URL... params) {
        Object result = null;
        try {
            URL url = (URL)params[0];
            this.urlConnection = (HttpURLConnection) url.openConnection();
            this.urlConnection.connect();
            //Controle du code réponse lors de la connection à l'URL
            if(urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                result = 1l;
            }
        } catch (java.io.IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    protected void onPostExecute(long result) {
        long r = result;
    }

    public void setUrl(String url) {
        try {
            this._url = new URL(url);
        }catch (java.net.MalformedURLException jmm){

        }
    }

    public WebService(Activity acitivity) {
        _activity = acitivity;
    }
}

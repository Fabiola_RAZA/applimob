package com.azou.fraz.applicationtholdi;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fraz on 16/03/2017.
 */

public class Reservation {

    //Champs privés
    private int _id;
    private String _dateReservation;
    private String _datePrevueStockage;
    private int _nbJoursDeStockagePrevu;
    private int _quantite;
    private String _etat;
    private int _numClient;

    //Propriétés
    public int get_id() {
        return _id;
    }

    public String get_dateReservation() {
        return _dateReservation;
    }
    public void set_dateReservation(String _dateReservation) {
        this._dateReservation = _dateReservation;
    }

    public String get_datePrevueStockage() {
        return _datePrevueStockage;
    }
    public void set_datePrevueStockage(String _datePrevueStockage) {
        this._datePrevueStockage = _datePrevueStockage;
    }

    public int get_nbJoursDeStockagePrevu() {
        return _nbJoursDeStockagePrevu;
    }
    public void set_nbJoursDeStockagePrevu(int _nbJoursDeStockagePrevu) {
        this._nbJoursDeStockagePrevu = _nbJoursDeStockagePrevu;
    }

    public int get_quantite() {
        return _quantite;
    }
    public void set_quantite(int _quantite) {
        this._quantite = _quantite;
    }

    public String get_etat() {
        return _etat;
    }
    public void set_etat(String _etat) {
        this._etat = _etat;
    }

    public int get_numClient() {
        return _numClient;
    }
    public void set_numClient(int _numClient) {
        this._numClient = _numClient;
    }

   //Constructeur
    public Reservation(int id, String dateReservation, String datePrevueStockage, int nbJourDeStockagePrevu, int quantite, String etat, int numClient) {
        _id = id;
        _dateReservation = dateReservation;
        _datePrevueStockage = datePrevueStockage;
        _nbJoursDeStockagePrevu = nbJourDeStockagePrevu;
        _quantite = quantite;
        _etat = etat;
        _numClient = numClient;
    }

    public ArrayList<Reservation> ParseJson() {
        ArrayList<Reservation> collectionReservation = new ArrayList<Reservation>();

        return collectionReservation;
    }
}
